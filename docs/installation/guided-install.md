# Guided installation

## Overview

We are so glad you want to install Xenia Linux! The installation can be boiled down to the following steps:

1. Download and boot live media
2. Download the installer
3. Run it!

## Booting live media

The installer relies on certain libraries that are not present in the Gentoo LiveCD, so currently we recommend the use of a [Fedora ISO](https://fedoraproject.org/workstation/download/) to use as an installation environment. It is likely other distros will work as well!

Once you are booted into live media, you should setup networking. On Fedora, you can do this in GNOME settings.

## Downloading the installer

Open a terminal and download the installer:

```{prompt}
:language: bash
:prompts: liveCD ~ \$
git clone https://gitlab.com/xenia-group/installer.git
```

Enter the installer's directory:

```{prompt}
:language: bash
:prompts: liveCD ~ \$
cd installer
```

## Run the installer

```{warning}
The installer won't run without root privileges! Make sure you use sudo here or become root through other means.
```

```{prompt}
:language: bash
:prompts: liveCD ~ \$
sudo ./main.py
```

```
[INFO] Entering interactive mode. Default values are shown wrapped in square brackets like [this]. Press enter to accept the default.

Enter value for 'drive'. Valid options are ['/dev/nvme0n1', '/dev/nvme1n1', '/dev/sda'].
[]: 
```

You will be put into interactive mode of the installer. When asking for values, the installer will give you a list of valid options. The default option will be shown in the square brackets.

### Drive

For the drive, pick your drive out of the valid options. If you don't know your drive, you can open another terminal and type `lsblk` to see all the drives on the system and how large they are.

### Repository

Stick to the default here unless rolling your own local repository. The instructions to do this are left as an exercise to the reader (hint: look at the repo structure).

### Flavour

Here, the flavour of Xenia to be installed can be selected. `gnome-systemd` is the most supported spin and is recommended. You may choose different distros, like `gnome-arch`. `systemd` images are Gentoo based, named as such due to Xenia previously being Gentoo only.

### Release branch

Release branch defines the branch of Xenia to use. It is important to note that Xenia is rolling, meaning that `unstable` and `main` will run the same package versions. This just defines changes to Xenia itself, such as packages being added to the base set or configuration changes being tested.

`main` is the only branch in use currently during the switchover to OCI/docker based builds.

### Filesystem

Here is where you choose which filesystem layout you would like. The default of btrfs is *heavily* recommended here, which gives features such as snapshots. Older versions of Xenia Linux used the LVM layout, and the traditional layout is just ext4 formatted partitions. You can find more information about the different layouts {doc}`here </architecture/fs-layouts>`.

To recap, please use the default of btrfs.

### Username

Enter a username here that you would like to use as your user. A password will be prompted for later.

### Applications

```{warning}
This feature is slightly broken, and only works on Gentoo-based images. Stick to minimal until further notice! 
```

Here, choose either recommended or minimal for packages to install. Recommended will install applications such as a web browser, image viewer, email client and the like with flatpak when you boot into the system. Minimal does not install any extra packages, and the user will need to install these packages themselves through a terminal or GNOME Software.

## Go!

With the installer configured, you'll see the following warning:

```
[WARN] Drive partitioning is about to start. After this process, drive /dev/sda will be erased. Press enter to continue.
```

When you are ready, press enter to begin the installation!

When the installer is complete, you can reboot into your new installation. You can login with the user you created earlier.

We hope you love Xenia Linux!
