# Install Xenia Linux

There is two ways to install Xenia Linux, {doc}`using a installer <guided-install>`, or {doc}`performing an installation manually <manual-install>`.

A guided install is easier and will guide you through the process. This is a fast method (disregarding download speeds, an install takes 15 seconds from boot to being at a login screen) and is the recommended method for most users.

A manual install is more similar to a typical Arch/Gentoo installation process, where you format your disks yourself, then install the base system (or the `root.img` in this case). A manual install doesn't provide much benefit apart from bragging rights and learning the internals of Xenia Linux and how it all works under the hood.

If you are in doubt, go with a guided install. If you are feeling adventurous, maybe choose a manual install! It's up to you.