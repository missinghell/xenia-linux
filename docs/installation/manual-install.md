# Manual installation

## Overview

This page will act as a rough guideline for installing Xenia Linux manually, without the use of an installer. While this won't show every different way you can setup your system, it will provide the necessary details to get a working system.

This guide will serve as a point-of-reference for those wanting to learn how a complete Xenia system is built and as a guide for those wanting to build their own custom system from scratch.

## Prerequisites

- [Gentoo live media](https://www.gentoo.org/downloads/)

## Booting the live media

Firstly, boot into the LiveCD and setup networking if applicable. To make sure this guide stays up-to-date, follow [the handbook](https://wiki.gentoo.org/wiki/Handbook:AMD64) up to Configuring the network.

## Setting up disks

### Xenia expectations

```{warning}
These docs are outdated, and mention filesystem layouts that have been deprecated. Use BTRFS only. No support will be given to other filesystems.
```

Xenia Linux expects a specific disk layout. For a traditional filesystem, it expects your filesystems to be labelled in a specific manner:

- ROOTS - this is where your root images are stored.
- OVERLAY - this is where system overlays over `/usr`, `/var` and `/etc` are stored.
- HOME - this is where `/home` resides.
- EFI - this is where `/boot/efi` resides.

For Btrfs, it expects the following labels:

- ROOTS - this is where root images, overlays and home is stored.
- EFI - this is where `/boot/efi` resides.

```{note}
You can find a full list of filesystem layouts {doc}`here </architecture/fs-layouts>`.
```

### Partitioning

First, find the disk to install on with `lsblk`:

```{prompt}
:language: bash
:prompts: liveCD ~ \#
lsblk
```
```
NAME MAJ:MIN RM SIZE RO TYPE MOUNTPOINTS
sda    8:0    0  80G  0 disk
```

Your disk will most likely be `/dev/sda`, `/dev/vda`, `/dev/nvme0n1` or `/dev/mmcblk0`. You can check the SIZE column to see if it matches with your disk.

Next, partition the disk with `fdisk`. Replace the disk referenced in the below commands with the one you found above. It should look like this, some text has been redacted for clarity:

```{prompt}
:language: bash
:prompts: liveCD ~ \#
fdisk /dev/sda
```
```
Welcome to fdisk (util-linux 2.38.1).

Command (m for help):
```

```{note}
For BIOS systems, we still make an "ESP" as that is where GRUB will store its config. It might be a bit confusing, but specific steps for BIOS/UEFI are marked throughout.
```

```{eval-rst}
.. tabs::

  .. group-tab:: Btrfs

    To partition the disk, we will follow the following layout:

    * ``/dev/sda1`` - EFI - 512M
    * ``/dev/sda2`` - ROOTS - remaining space

    To achieve this layout with ``fdisk``, use the following commands:

    .. tabs::

      .. group-tab:: UEFI

        ::

          g
          n
          (press enter)
          (press enter)
          +512M
          n
          (press enter)
          (press enter)
          (press enter)
          w

      .. group-tab:: BIOS

        ::

          g
          n
          (press enter)
          (press enter)
          +512M
          n
          (press enter)
          (press enter)
          -1M
          n
          (press enter)
          (press enter)
          (press enter)
          t
          (press enter)
          4
          w

  .. group-tab:: ext4

    To partition the disk, we will follow the following layout:

    * ``/dev/sda1`` - EFI - 512M
    * ``/dev/sda2`` - ROOTS - 8G
    * ``/dev/sda3`` - OVERLAY - 20G
    * ``/dev/sda4`` - HOME - remaining space

    .. note:: As a rule of thumb, assign at least 4G to ROOTS (otherwise you will not be able to upgrade the system), and around 30% of your remaining space to OVERLAY. The rest can be assigned to ``/home``.

    To achieve this layout with ``fdisk``, use the following commands:

    .. tabs::

      .. group-tab:: UEFI

        ::

          g
          n
          (press enter)
          (press enter)
          +512M
          n
          (press enter)
          (press enter)
          +8G
          n
          (press enter)
          (press enter)
          +20G
          n
          (press enter)
          (press enter)
          (press enter)
          w

      .. group-tab:: BIOS

        ::

          g
          n
          (press enter)
          (press enter)
          +512M
          n
          (press enter)
          (press enter)
          +8G
          n
          (press enter)
          (press enter)
          +20G
          n
          (press enter)
          (press enter)
          -1M
          n
          (press enter)
          (press enter)
          (press enter)
          t
          (press enter)
          4
          w

```

### Formatting

First, format the EFI System Partition:

```{prompt}
:language: bash
:prompts: liveCD ~ \#
mkfs.vfat -F 32 -n EFI /dev/sda1
```

```{eval-rst}
.. tabs::

  .. group-tab:: Btrfs

    Next, format the Btrfs partition:

    .. prompt::
      :language: bash
      :prompts: liveCD ~ #

      mkfs.btrfs -L ROOTS /dev/sda2

    With our disks partitioned, we will mount ROOTS to prepare for the `root.img`:

    .. prompt::
      :language: bash
      :prompts: liveCD ~ #

      mount -L ROOTS /mnt/gentoo
      cd /mnt/gentoo

    Next, we will create the subvolumes for `/home` and the filesystem overlays.

    .. prompt::
      :language: bash
      :prompts: liveCD ~ #

      btrfs subvolume create /mnt/gentoo/home
      btrfs subvolume create /mnt/gentoo/overlay
      btrfs subvolume create /mnt/gentoo/overlay/etc
      btrfs subvolume create /mnt/gentoo/overlay/var
      btrfs subvolume create /mnt/gentoo/overlay/usr

  .. group-tab:: ext4

    Next, format the rest of the partitions as ext4:

    .. prompt::
      :language: bash
      :prompts: liveCD ~ #

      mkfs.ext4 -L ROOTS /dev/sda2
      mkfs.ext4 -L OVERLAY /dev/sda3
      mkfs.ext4 -L HOME /dev/sda4

    With our disks partitioned, we will mount ROOTS to prepare for the `root.img`:

    .. prompt::
      :language: bash
      :prompts: liveCD ~ #

      mount -L ROOTS /mnt/gentoo
      cd /mnt/gentoo

```

## Acquiring a `root.img`

The root image is the heart of a Xenia Linux system. It contains your whole root filesystem. 

### Picking the root image

There are a few versions of the root image. The main two are:

- [Current](https://repo.xenialinux.com/releases/current/), which is the current stable release of Xenia. Some newer features may not be available, but this is a tested point release
- [Unstable](https://repo.xenialinux.com/releases/unstable/), which is the latest rolling version of Xenia. This may contain some bugs but will have newer features being prepared for a later release

As a rule of thumb, pick Current if you are new to Xenia Linux or want a more stable, tested system.

### Acquiring the root image

#### Downloading the root in the live media

You can download the root image now. Depending on what release channel you would like, substitute `current` for `unstable`:

```{eval-rst}
.. tabs::

  .. group-tab:: systemd

    .. prompt::
      :language: bash
      :prompts: liveCD /mnt/gentoo #

      wget https://repo.xenialinux.com/releases/current/root-systemd.img
      mv /mnt/gentoo/root-systemd.img /mnt/gentoo/root.img

  .. group-tab:: OpenRC

    .. warning:: OpenRC is deprecated (for now) in 0.5 Cana. These images may not work or be present.

    .. prompt::
      :language: bash
      :prompts: liveCD /mnt/gentoo #

      wget https://repo.xenialinux.com/releases/current/root.img
```


#### Building your own

To learn about building your own root follow the guide {doc}`here </development/create-root>`. When you get to the Installing section, host the web server and copy the root over to here.

## Setting up the new system

First, make the directory `/mnt/root`:

```{prompt}
:language: bash
:prompts: liveCD /mnt/gentoo \#
mkdir /mnt/root
cd /mnt/root
```

And mount `root.img` there:

```{prompt}
:language: bash
:prompts: liveCD /mnt/gentoo \#
mount -o ro,loop -t squashfs /mnt/gentoo/root.img /mnt/root
```

Mount the ESP:

```{prompt}
:language: bash
:prompts: liveCD /mnt/gentoo \#
mount -L EFI /mnt/root/boot/efi
```

Mount home:

```{eval-rst}
.. tabs::

  .. group-tab:: Btrfs

    .. prompt::
      :language: bash
      :prompts: liveCD /mnt/gentoo #

      mount -L ROOTS -o subvol=home /mnt/root/home

  .. group-tab:: ext4

    .. prompt::
      :language: bash
      :prompts: liveCD /mnt/gentoo #

      mount -L HOME /mnt/root/home
```

## Chroot into Xenia and install the bootloader

Next, we will chroot into the new system and install the bootloader.

Mount special filesystems:

```{prompt}
:language: bash
:prompts: liveCD /mnt/gentoo \#
mount -t proc /proc /mnt/root/proc
mount --rbind /dev /mnt/root/dev
mount --rbind /sys /mnt/root/sys
mount --bind /run /mnt/root/run
mount --make-slave /mnt/root/run
```

Chroot into the new system:

```{prompt}
:language: bash
:prompts: liveCD /mnt/gentoo \#
chroot /mnt/root /bin/bash
```

```{note}
We install grub with LVM support here, as LVM layouts won't boot without it. Feel free to leave it out if you aren't using LVM.
```

Install the bootloader:

```{eval-rst}
.. tabs::

  .. group-tab:: UEFI

    .. prompt::
      :language: bash
      :prompts: liveCD /mnt/gentoo #

      grub-install --modules=lvm --target="x86_64-efi" --efi-directory="/boot/efi" --boot-directory="/boot/efi"

  .. group-tab:: BIOS

    .. prompt::
      :language: bash
      :prompts: liveCD /mnt/gentoo #

      grub-install --modules=lvm  --boot-directory="/boot/efi" /dev/sda  # Enter your device you determined above here
```

Configure the bootloader:

```{prompt}
:language: bash
:prompts: liveCD /mnt/gentoo \#
grub-mkconfig -o /boot/efi/grub/grub.cfg
```

```{warning}
These docs are outdated and do not show how to create a user - in summary, mount the etc overlay (/roots/overlay/etc/etc) and make a user like normal while in the chroot. Help is appreciated if you would like to flesh out these docs!
```

And you are done! Now for the moment of truth:

```{prompt}
:language: bash
:prompts: liveCD /mnt/gentoo \#
reboot
```

Now, time to `reboot`.