# OpenRC flatpak workaround

In OpenRC, flatpaks do not open properly if they are DBus enabled, as the DBus service file [isn't able to be found](https://gitlab.com/xenia-group/catalyst/-/issues/5).

To fix this, create a symlink like this:

```{prompt}
:language: bash
:prompts: xenia ~ \$
ln -sf /var/lib/flatpak/exports/share/dbus-1/ /home/$USER/.local/share/
```