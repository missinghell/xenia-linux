# Filesystem layouts 

Xenia Linux supports a wide range of filesystem layouts and types, but has certain requirements to boot. This page will show the current supported layouts and what is needed to boot.

## Traditional label-based layout (generic)


```{warning}
Outdated. Use BTRFS.
```

This layout is the current layout used for LVM setups and can be used on any filesystem that supports labels.

### Physical map

```
d1
├─d1p1 - EFI - 512M
├─d1p2 - ROOTS - 8G
├─d1p3 - OVERLAY - around 30% remaining space
└─d1p4 - HOME - all remaining space
```

### Description

This layout contains 4 partitions:
- p1 labelled EFI for grub
- p2 labelled ROOTS for root image storage
- p3 labelled OVERLAY for filesystem overlays
- p4 labelled HOME for `/home`

EFI, ROOTS, OVERLAY and HOME labels are necessary for Xenia to boot this layout. If OVERLAY is not present, Xenia will boot into recovery.

## Traditional label-based layout (LVM)

```{warning}
Outdated. Use BTRFS.
```

### Physical map

```
d1
├─d1p1 - EFI - 512M
└─d1p2 - LVM - all remaining space
```

### Logical map

```
d1p2
├─vg0-roots - ROOTS - 8G
├─vg0-overlay - OVERLAY - around 30% remaining space
└─vg0-home - HOME - all remaining space
```

### Description

This layout contains 2 partitions:
- p1 labelled EFI for grub
- p2 for LVM

This layout contains 3 LVs:
- vg0-roots labelled ROOTS for root image storage
- vg0-overlay labelled OVERLAY for filesystem overlays
- vg0-home labelled HOME for `/home`

EFI, ROOTS, OVERLAY and HOME labels are necessary for Xenia to boot this layout.

## Btrfs

This layout is for Btrfs based systems.

### Physical map

```
d1
├─d1p1 - EFI - 512M
└─d1p2 - ROOTS - all remaining space
```

### Logical map

```
d1p2 - ROOTS
├─overlay - btrfs subvolume
│ ├─etc - btrfs subvolume
│ ├─var - btrfs subvolume
│ └─usr - btrfs subvolume
└─home - btrfs subvolume
```

### Description

This layout contains 2 partitions:
- p1 labelled EFI for grub
- p2 for Btrfs labelled ROOTS

This layout contains 5 subvolumes alongside the root of the Btrfs:
- root of the Btrfs used for root image storage
- overlay for filesystem overlays
  - etc for etc overlay
  - var for var overlay
  - usr for usr overlay
- home for `/home`

BTRFS needs to have the root images stored in its root, not in a subvolume. overlay, etc, var, usr and home need to be named as such for Xenia to mount them.

```{note}
Overlays on a btrfs system look like this:  
`/overlay/etc/etc /overlay/etc/etcw`  
`/overlay/var/var /overlay/etc/etcw`  
due to both the upper directory and work directory having to be on the same filesystem. Don't worry about making these yourself (foxmount will do it for you on first boot), just make the subvolumes.
```

## Btrfs with encryption

This layout is for Btrfs/LUKS based systems.

### Physical map

```
d1
├─d1p1 - EFI - 512M
├─d1p2 - ROOTS - 8G
└─d1p3 - XENIA - all remaining space
```

### Logical map

```
d1p3 - XENIA (crypt)
├─overlay - btrfs subvolume
│ ├─etc - btrfs subvolume
│ ├─var - btrfs subvolume
│ └─usr - btrfs subvolume
└─home - btrfs subvolume
```

### Description

This layout contains 3 partitions:
- p1 labelled EFI for grub
- p2 labelled ROOTS for root image storage
- p3 for Btrfs/LUKS labelled XENIA

This layout contains 5 subvolumes alongside the root of the Btrfs:
- overlay for filesystem overlays
  - etc for etc overlay
  - var for var overlay
  - usr for usr overlay
- home for `/home`

This layout comprises of a ext4/xfs/any other labelled filesystem for root image storage, and a LUKS encrypted partition formatted as Btrfs for overlays and home.

```{note}
Overlays on a btrfs system look like this:  
`/overlay/etc/etc /overlay/etc/etcw`  
`/overlay/var/var /overlay/etc/etcw`  
due to both the upper directory and work directory having to be on the same filesystem. Don't worry about making these yourself (foxmount will do it for you on first boot), just make the subvolumes.
```
