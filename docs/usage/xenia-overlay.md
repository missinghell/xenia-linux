# Add the Xenia portage overlay

```{warning}
The portage overlay is only available on Gentoo-based images.
```

The Xenia portage overlay includes multiple tools such as foxsnapshot to be used on a Xenia system.

For development reasons (or to {doc}`create a Xenia root image </development/create-root>`), the repository can be added to a Gentoo or Xenia system.

## Xenia specific steps

If Xenia Linux is being used, first sync the Gentoo repositories:

```{note}
The command `emerge-webrsync` is used here as it will fix issues with using emerge in the next step.
```

```{prompt}
:language: bash
:prompts: xenia ~ \#
emerge-webrsync
```

Next, mount `/usr` as read-write:

```{prompt}
:language: bash
:prompts: xenia ~ \#
mount -o rw,remount /usr
```

## Installing eselect-repository

Install `eselect-repository`:

```{prompt}
:language: bash
:prompts: xenia ~ \#
emerge -va eselect-repository
```

## Add the overlay

First, add the overlay:

```{prompt}
:language: bash
:prompts: xenia ~ \#
eselect repository add xenia-overlay git https://gitlab.com/xenia-group/xenia-overlay
```

And sync it:

```{prompt}
:language: bash
:prompts: xenia ~ \#
emaint sync --repo xenia-overlay
```

Done!

## Allowing live ebuilds

In their current state, most fox-utils are only available as live ebuilds. 

To allow `emerge` to use these, add the following to `/etc/portage/package.accept_keywords/xenia`:

```
xenia-tools/* **
```
