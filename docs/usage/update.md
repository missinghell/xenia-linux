# Updating the system

```{warning}
`foxupdate` is only available on Gentoo-based images.
```

## Prerequisites for 0.4 Velox

On 0.4, the update utility `foxupdate` will not be installed.

First, {doc}`install the Xenia overlay </usage/xenia-overlay>`.

Next, run the following command to install `foxupdate`:

```{prompt}
:language: bash
:prompts: xenia ~ \#
mount -o rw,remount /usr
emaint sync --repo xenia-overlay
emerge -va1 foxupdate
```

## First-time setup

First, run `foxupdate`:

```{prompt}
:language: bash
:prompts: xenia ~ \#
foxupdate
```

You will be run through a number of prompts.

- `repository`: This is the URL where the root images are hosted. Unless you are testing your own images, leave this as the default.
- `flavour`: This is what flavour (init system, desktop environment) will be used. As of 0.5, only `gnome-systemd` will be provided.
- `release_branch`: This is what release you want to download. Either use `main` for a stable, tested point release or `unstable` to get the latest features.

If you see an option called `date`, please follow the above instructions to install/update `foxupdate` to the latest version.

`foxupdate` will now show an output showing your choices:

```
[INFO] Config setup!

foxupdate
---------

Repository: https://repo.xenialinux.com/releases/
Flavour: gnome-systemd
Release branch[sub-branch]: unstable

Run foxupdate -u to update!
```

If you would like to change these later, run `foxupdate -c`

## Updating

To update, simply run the following:

```{prompt}
:language: bash
:prompts: xenia ~ \#
foxupdate -u
```

`foxupdate` will only replace the existing root once it has fully downloaded the new one, and when done backs up the existing root to `root.img.bak`.

If an unexpected event occurs where the new image is not bootable, follow the Troubleshooting section {doc}`here </usage/manual-update>`.

