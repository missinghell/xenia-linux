# Installing applications

On Xenia, the way to install applications is different from the way traditional distributions work. In Xenia, the preferred installation method for GUI applications is Flatpak, for CLI application is Distrobox, and finally the `/usr` overlay for any packages needed on the base system.

## Flatpak

Flatpak is pre-installed and setup ready-to-go. To install applications with Flatpak, you can:

- Use the GUI tool GNOME Software
- Use the CLI

GNOME Software is pre-installed, and it is accessed by searching for "software" in Activities or finding in application view of GNOME.

To use the CLI to install an application, for example Firefox:
```{prompt}
:language: bash
:prompts: xenia ~ \$
flatpak install firefox
```

```
Looking for matches…
Similar refs found for ‘firefox’ in remote ‘flathub’ (system):

   1) app/org.mozilla.firefox.BaseApp/x86_64/20.08
   2) app/org.mozilla.firefox.BaseApp/x86_64/21.08
   3) app/org.mozilla.firefox/x86_64/stable
   4) app/org.mozilla.firefox.BaseApp/x86_64/22.08

Which do you want to use (0 to abort)? [0-4]: 
```

Here, you can pick the application to install. In this case, pick 3.

On OpenRC, any Flatpak which launches through DBus (mainly GNOME flatpaks) will not open. Follow the workaround {doc}`here </misc/openrc-flatpak>` if this occurs to you.

## Distrobox

For CLI applications or applications that are not on Flatpak, Xenia ships with Distrobox, a tool to manage containers of other distributions.

Distrobox is similar to Toolbox on Fedora, but gives you access to any distribution you would like. However, we will use the default of Fedora for this guide. You can learn more [here](https://github.com/89luca89/distrobox/tree/main).

### Setup

First, setup a Distrobox:

```{prompt}
:language: bash
:prompts: xenia ~ \$
distrobox enter
```

Go through the prompts (you can say yes to everything) to setup a Fedora container. You will now be inside the container.

### Installing applications

To install applications, you can use the distributions package manager, in this case DNF:

```{prompt}
:language: bash
:prompts: "[xenia@my-distrobox ~]$"
sudo dnf install emacs
```

### Exporting to the host

Distrobox can also export applications to the host. For example, you could install an application like virt-viewer in the container. After this, you can export to the host:

```{prompt}
:language: bash
:prompts: "[xenia@my-distrobox ~]$"
distrobox-export --app virt-viewer
```

### Virt-manager (KVM)

A common use-case is using KVM in distrobox instead of cluttering the base system. You can follow the guide {doc}`here <installing-vmm>` to set this up.

Alternatively, install GNOME Boxes from Flatpak for a hassle-free solution.

## `foxmerge`

```{note}
`foxmerge` was officially introduced in 0.5 Cana. Please {doc}`update your system </usage/update>` if needed.
```

For applications like VPNs, or drivers such as the proprietary Nvidia drivers, Flatpak or Distrobox will not work.

```{warning}
`foxmerge` is only available on Gentoo-based images.
```

`foxmerge` allows for Portage to be used in Xenia. First, add a package:

```{prompt}
:language: bash
:prompts: xenia ~ \#
foxmerge add emacs
```

Then, run an upgrade:

```{prompt}
:language: bash
:prompts: xenia ~ \#
foxmerge upgrade
```

To remove a package:

```{prompt}
:language: bash
:prompts: xenia ~ \#
foxmerge remove emacs
```

On Btrfs systems, `foxmerge` will create snapshots before and after upgrades and removals. If an operation fails, a revert operation will be scheduled.

If something trivial goes wrong, such as a package being keyworded unstable, it is safe to run `foxsnapshot revert --cancel` to cancel the revert, and try again.

As a rule of thumb, do not try to install lots of packages with foxmerge (only the ones needed) or try to update the system through it - this will lead to package conflicts in the future.
