# Updating the system - Manual

```{warning}
If the system is currently on 0.4 Velox, please follow the guide {doc}`here </usage/update>` to update the system. Updating manually will result in an unbootable system!
```

## Preparation

Firstly, mount `/roots` as read-write, and enter the directory:

```{prompt}
:language: bash
:prompts: xenia ~ \#
mount -o rw,remount /roots
cd /roots
```

Backup the current root:

```{prompt}
:language: bash
:prompts: xenia /roots \#
mv /roots/root.img /roots/root.img.bak
```

## Download the new root

```{note}
Xenia can switch between OpenRC and systemd at will! Use whichever init system you want.
```

```{warn}
Xenia only looks for a file called `root.img`, and will not boot by default if it cannot find this file. Make sure to rename the file before rebooting.
```

```{eval-rst}
.. tabs::

  .. group-tab:: systemd

    .. prompt::
      :language: bash
      :prompts: xenia /roots #

      wget https://repo.xenialinux.com/releases/current/root-systemd.img
      mv /roots/root-systemd.img /roots/root.img

  .. group-tab:: OpenRC

    .. warning:: OpenRC is deprecated (for now) in 0.5 Cana. These images may not work or be present.

    .. prompt::
      :language: bash
      :prompts: xenia /roots #

      wget https://repo.xenialinux.com/releases/current/root.img
```

## Finalising

Reboot:

```{prompt}
:language: bash
:prompts: xenia /roots \#
reboot
```

Delete the old root if it is no longer needed:

```{prompt}
:language: bash
:prompts: xenia ~ \#
rm /roots/root.img.bak
```

## Troubleshooting

### `error: file 'root.img' not found`, booting backup root

This error indicates grub cannot find the `root.img`, indicating it no longer exists on `/roots` (or is corrupted).

If a backup was made of the previous root, it can be booted by editing the GRUB menuentry:

Firstly, press enter to continue, then press e to enter the editor. From here, you'll see a menuentry like so:

```{code-block}
:linenos:
:emphasize-lines: 4,12
menuentry 'Xenia Linux'

	search.fs_label ROOTS roots
	loopback loop0 ($roots)/root.img
	load_video
	if [ "x$grub_platform" = xefi ]; then
		set gfxpayload=keep
	fi
	insmod gzio
	insmod squash4
	echo	'Loading Linux gentoo-dist ...'
	linux	(loop0)/boot/vmlinuz ro quiet splash rd.live.squashimg=root.img rd.live.dir=/ rd.live.overlay.overlayfs=1 root=live:/dev/disk/by-label/ROOTS recovery=false
	echo	'Loading initial ramdisk ...'
	initrd	(loop0)/boot/initramfs.img
```

From here, change the occurences of `root.img` to `root.img.bak`:

```{code-block}
:linenos:
:emphasize-lines: 4,12
menuentry 'Xenia Linux'

	search.fs_label ROOTS roots
	loopback loop0 ($roots)/root.img.bak
	load_video
	if [ "x$grub_platform" = xefi ]; then
		set gfxpayload=keep
	fi
	insmod gzio
	insmod squash4
	echo	'Loading Linux gentoo-dist ...'
	linux	(loop0)/boot/vmlinuz ro quiet splash rd.live.squashimg=root.img.bak rd.live.dir=/ rd.live.overlay.overlayfs=1 root=live:/dev/disk/by-label/ROOTS recovery=false
	echo	'Loading initial ramdisk ...'
	initrd	(loop0)/boot/initramfs.img
```

Then, press ctrl+x to boot.

### Entering emergency mode (0.4 to 0.5 update)

Dracut made a change where the old kernel cmdline we used no longer works in Xenia. If you updated *manually* to 0.5, the system will no longer boot.

First, enter the GRUB editor by pressing `e` when hovering over Xenia Linux from the GRUB menu. A menuentry will be shown:

```{code-block}
:linenos:
:emphasize-lines: 12
menuentry 'Xenia Linux'

	search.fs_label ROOTS roots
	loopback loop0 ($roots)/root.img
	load_video
	if [ "x$grub_platform" = xefi ]; then
		set gfxpayload=keep
	fi
	insmod gzio
	insmod squash4
	echo	'Loading Linux gentoo-dist ...'
	linux	(loop0)/boot/vmlinuz ro quiet splash rd.live.squashimg=root.img rd.live.dir=/ rd.live.overlay.overlayfs=0 root=live:/dev/disk/by-label/ROOTS recovery=false
	echo	'Loading initial ramdisk ...'
	initrd	(loop0)/boot/initramfs.img
```

Change `rd.live.overlay.overlayfs=0` to `rd.live.overlay.overlayfs=1`:

```{code-block}
:linenos:
:emphasize-lines: 12
menuentry 'Xenia Linux'

	search.fs_label ROOTS roots
	loopback loop0 ($roots)/root.img
	load_video
	if [ "x$grub_platform" = xefi ]; then
		set gfxpayload=keep
	fi
	insmod gzio
	insmod squash4
	echo	'Loading Linux gentoo-dist ...'
	linux	(loop0)/boot/vmlinuz ro quiet splash rd.live.squashimg=root.img rd.live.dir=/ rd.live.overlay.overlayfs=1 root=live:/dev/disk/by-label/ROOTS recovery=false
	echo	'Loading initial ramdisk ...'
	initrd	(loop0)/boot/initramfs.img
```

Then, press ctrl+x to boot.

#### Fixing it the easy way

Follow the guide {doc}`here </usage/update>` to update with `foxupdate`.

#### Fixing it the hard way

When booted, first mount `/boot/efi`:

```{prompt}
:language: bash
:prompts: xenia ~ \#
mount -L EFI /boot/efi
```

Open `/boot/efi/grub/grub.cfg` in a text editor as root, and find the menuentry for Xenia Linux:

```{code-block}
:linenos:
:emphasize-lines: 11,26
menuentry 'Xenia Linux' --class xenia --class gnu-linux --class gnu --class os $menuentry_id_option 'gnulinux-simple-/mnt/roots/root.img' {
        search.fs_label ROOTS roots
        loopback loop0 ($roots)/root.img
        load_video
        if [ "x$grub_platform" = xefi ]; then
                set gfxpayload=keep
        fi
        insmod gzio
        insmod squash4
        echo    'Loading Linux gentoo-dist ...'
        linux   (loop0)/boot/vmlinuz ro quiet splash rd.live.squashimg=root.img rd.live.dir=/ rd.live.overlay.overlayfs=0 root=live:/dev/disk/by-label/ROOTS recovery=false
        echo    'Loading initial ramdisk ...'
        initrd  (loop0)/boot/initramfs.img
}

menuentry 'Xenia Linux (Recovery)' --class xenia --class gnu-linux --class gnu --class os $menuentry_id_option 'gnulinux-simple-/mnt/roots/root.img' {
        search.fs_label ROOTS roots
        loopback loop0 ($roots)/root.img
        load_video
        if [ "x$grub_platform" = xefi ]; then
                set gfxpayload=keep
        fi
        insmod gzio
        insmod squash4
        echo    'Loading Linux gentoo-dist ...'
        linux   (loop0)/boot/vmlinuz ro quiet splash rd.live.squashimg=root.img rd.live.dir=/ rd.live.overlay.overlayfs=0 root=live:/dev/disk/by-label/ROOTS recovery=true
        echo    'Loading initial ramdisk ...'
        initrd  (loop0)/boot/initramfs.img
}
```

Change the `rd.live.overlay.overlayfs=0` to `rd.live.overlay.overlayfs=1` on both lines:

```{code-block}
:linenos:
:emphasize-lines: 11,26
menuentry 'Xenia Linux' --class xenia --class gnu-linux --class gnu --class os $menuentry_id_option 'gnulinux-simple-/mnt/roots/root.img' {
        search.fs_label ROOTS roots
        loopback loop0 ($roots)/root.img
        load_video
        if [ "x$grub_platform" = xefi ]; then
                set gfxpayload=keep
        fi
        insmod gzio
        insmod squash4
        echo    'Loading Linux gentoo-dist ...'
        linux   (loop0)/boot/vmlinuz ro quiet splash rd.live.squashimg=root.img rd.live.dir=/ rd.live.overlay.overlayfs=1 root=live:/dev/disk/by-label/ROOTS recovery=false
        echo    'Loading initial ramdisk ...'
        initrd  (loop0)/boot/initramfs.img
}

menuentry 'Xenia Linux (Recovery)' --class xenia --class gnu-linux --class gnu --class os $menuentry_id_option 'gnulinux-simple-/mnt/roots/root.img' {
        search.fs_label ROOTS roots
        loopback loop0 ($roots)/root.img
        load_video
        if [ "x$grub_platform" = xefi ]; then
                set gfxpayload=keep
        fi
        insmod gzio
        insmod squash4
        echo    'Loading Linux gentoo-dist ...'
        linux   (loop0)/boot/vmlinuz ro quiet splash rd.live.squashimg=root.img rd.live.dir=/ rd.live.overlay.overlayfs=1 root=live:/dev/disk/by-label/ROOTS recovery=true
        echo    'Loading initial ramdisk ...'
        initrd  (loop0)/boot/initramfs.img
}
```

Save the file.

