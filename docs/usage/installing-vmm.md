# Installing Virtual Machine Manager

```{warning}
This documentation is deprecated. Please use GNOME Boxes if possible.
```

Virtual Machine Manger, or `virt-manager`, is a popular GUI front-end to libvirt (and KVM/QEMU) on Linux machines. Installing `virt-manager` is traditionally done on the base system, but this is not advised in Xenia due to potential conflicts (the rule of thumb is to not use the `/usr` overlay if necessary).

Luckily, it is possible to install `virt-manager` on a distrobox, and export it to the host to use virtual machines on Xenia.

## Creating the container

First, create a rootful distrobox with a container that supports init systems - the example given is `registry.opensuse.org/opensuse/tumbleweed:latest`:

```{note}
The container can be named anything, here it is named libvirt.
```

```{prompt}
:language: bash
:prompts: xenia ~ \$
distrobox create --root --init --image registry.opensuse.org/opensuse/tumbleweed:latest --name libvirt
```

Next, enter the container

```{prompt}
:language: bash
:prompts: xenia ~ \$
distrobox enter --root libvirt
```

## Installing virt-manager

Install the kvm tools and server:

```{prompt}
:language: bash
:prompts: "xenia@libvirt:~>"
sudo zypper install -t pattern kvm_server kvm_tools
```

Install virt-manager:

```{prompt}
:language: bash
:prompts: "xenia@libvirt:~>"
sudo zypper install virt-manager
```

## Starting required services & exporting

Start libvirtd:

```{prompt}
:language: bash
:prompts: "xenia@libvirt:~>"
sudo systemctl enable --now libvirtd
```

Export `virt-manager`:

```{prompt}
:language: bash
:prompts: "xenia@libvirt:~>"
distrobox-export --app virt-manager
```

Virtual Machine Manager is now installed, and it is able to be opened through GNOME like usual - but we still need to setup the QEMU/KVM session.

## Setup the session

Open Virtual Machine Manager through the activites overview on GNOME.

```{note}
If the container is not started when you open VMM, the startup might take a while. Be patient!
```

Go to `File > Add Connection...`. From here, change the Hypervisor to `QEMU/KVM user session`, and click Connect.

Done! Now VMM is installed and ready to be used.