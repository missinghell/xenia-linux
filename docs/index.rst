.. Xenia Linux documentation master file, created by
   sphinx-quickstart on Fri Aug 11 17:23:16 2023.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Xenia Linux's documentation!
=======================================

Xenia Linux is an immutable OS based on Gentoo Linux. Xenia Linux boots from a SquashFS (the root), much like how a LiveCD works.

Xenia Linux includes the GNOME desktop environment, a selection of filesystems, and versatility with Flatpak, Distrobox and Portage available to install packages with. Xenia Linux includes your entire system on one file, which can be swapped out for a newer one when updates come.

We named Xenia Linux after the popular Linux mascot who lost to Tux.

The documentation contains guides on installing the system (guided or manual installations), using the system, developing the system and system architecture.

.. toctree::
   :maxdepth: 2
   :caption: Installation

   /installation/index
   /installation/guided-install
   /installation/manual-install

.. toctree::
   :maxdepth: 2
   :caption: Disks

   /disks/btrfs

.. toctree::
   :maxdepth: 2
   :caption: Usage

   /usage/installing-apps
   /usage/update
   /usage/manual-update
   /usage/xenia-overlay
   /usage/installing-vmm

.. toctree::
   :maxdepth: 2
   :caption: Development

   /development/create-root-guided
   /development/create-root
   /development/customise-root

.. toctree::
   :maxdepth: 2
   :caption: Architecture

   /architecture/fs-layouts

.. toctree::
   :maxdepth: 2
   :caption: Miscellaneous

   /misc/openrc-flatpak

