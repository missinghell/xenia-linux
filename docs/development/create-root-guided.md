# RootFS generation

```{warning}
These docs are outdated, using catalyst for builds. Our current Docker build system hasn't been documented yet - help appreciated!
```

To make custom Xenia images, the tool `foxbuild` can be used on a Gentoo system/container or as its own container.

## Xenia setup

As Xenia is immutable and it is advised to not install to the base system when necessary, distrobox will be used. This can also be followed on any system with distrobox.

First, setup a Gentoo distrobox and enter it:

```{prompt}
:language: bash
:prompts: xenia ~ \#
distrobox create --root --image docker.io/gentoo/stage3 --name gentoo
distrobox enter --root gentoo
```

Setup the container:

```{prompt}
:language: bash
:prompts: gentoo ~ \#
emerge --sync
```

The remainder of this guide takes place from within the Gentoo distrobox.

## `foxbuild` installation

First, {doc}`install the Xenia overlay </usage/xenia-overlay>`.

Next, allow for `foxbuild` to be emerged:

```{prompt}
:language: bash
:prompts: gentoo ~ \#
echo "xenia-tools/foxbuild **" >> /etc/portage/package.accept_keywords/foxbuild
```

Install `foxbuild`:

```{prompt}
:language: bash
:prompts: gentoo ~ \#
emerge foxbuild
```

### Initial setup

Run the following commands:

```{prompt}
:language: bash
:prompts: gentoo ~ \#
mkdir -p /var/foxbuild/output
touch /var/foxbuild/output/Manifest.toml
```

## Creating a root image

`foxbuild` pulls from a git repo to get the required files to build. First, we will use the existing repo that Xenia builds from.

Run `foxbuild`:

```{prompt}
:language: bash
:prompts: gentoo ~ \#
foxbuild
```

Configuration options will appear.

### catalyst_repo

Set the git repo to pull from here. Here, we will use the default of `https://gitlab.com/xenia-group/catalyst.git`.

### git_branch

The git branch from the repo set above to use. Here, we will use the default of unstable.

### target

Here, the target set to be built can be specified. This will typically be a spec file such as `stage4-systemd.spec`, but can also be a comma seperated list of spec files, `changed` to only rebuild spec files that have changed in the last commit and `*` to build all targets. We will leave this at the default `stage4-systemd.spec`.

### Using a config file

`foxbuild` can also take a config file instead of running interactively.

To do this, write a config file, here called `build.toml`:

```toml
catalyst_repo = "https://gitlab.com/xenia-group/catalyst.git"
git_branch = "unstable"
target = "stage4-systemd.spec"
```

Then, run `foxbuild` with `-c` specified:

```{prompt}
:language: bash
:prompts: gentoo ~ \#
foxbuild -c build.toml
```
---

Now, `foxbuild` will start setting up the build envionment, generating a Portage snapshot and downloading the required seed stage3 for the spec selected. It will also show where it is logging. To see the log in real-time, open another terminal and run `tail -f` on the log:

```{prompt}
:language: bash
:prompts: gentoo ~ \$
tail -f /var/foxbuild/stage4-systemd.spec.log
```

## Testing the image

`foxbuild` creates a repo in `/var/foxbuild/output`. First, expose the repo over a web server:

```{prompt}
:language: bash
:prompts: gentoo ~ \$
cd /var/foxbuild/output
python3 -m http.server
```

Next, from the installer or using `foxupdate` on a Xenia system, set the repository to `http://localhost:8000/` (or replace localhost with the IP/domain of the system hosting the repo).

Then, you can select the available targets that `foxbuild` has built.

## Updating the build environment

`foxbuild` generates a Portage snapshot if there isn't already one on the system, but doesn't create a new one every time it is run. If the `git_repo` is changed, `foxbuild` will not pick this up until the build environment has been updated. To update the build environment, run `foxbuild` with `-u`:

```{prompt}
:language: bash
:prompts: gentoo ~ \#
foxbuild -uc build.toml
```

## Customising the image

First, create a fork of [the catalyst repo](https://gitlab.com/xenia-group/catalyst). Then, follow the customisation guide {doc}`here </development/customise-root>` to customise the root.

Once changes have been made, push them to the repo. Then, set the `repository` in `foxbuild` config to match the repo created.
