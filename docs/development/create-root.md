# Manual rootFS generation

```{warning}
These docs are outdated, using catalyst for builds. Our current Docker build system hasn't been documented yet - help appreciated!
```

We use catalyst to generate our rootFS images. Catalyst is a build tool made by Gentoo which is used to build Gentoo (and anything based on it). This page will walk you through making a root image of your own.

## Requirements

- Gentoo installation (or chroot)
- [`app-arch/pixz`](https://packages.gentoo.org/packages/app-arch/pixz) and [`sys-fs/squashfs-tools`](https://packages.gentoo.org/packages/sys-fs/squashfs-tools) installed
- Patience

## First time setup

First, install `dev-util/catalyst` on your system.

```{note}
`dev-util/catalyst` may be masked when you read this. Make sure to unmask it by adding `dev-util/catalyst **` to `/etc/portage/package.accept_keywords/catalyst`
```

Create the directory `/var/tmp/catalyst/builds/default`:

```{prompt}
:language: bash
:prompts: xenia ~ \#
mkdir -p /var/tmp/catalyst/builds/default
```

Make sure `/var/cache/distfiles` is created:

```{prompt}
:language: bash
:prompts: xenia ~ \#
mkdir -p /var/cache/distfiles
```

Next, clone our catalyst repository:

```{prompt}
:language: bash
:prompts: xenia ~ \$
git clone https://gitlab.com/xenia-group/catalyst
```

Enter the directory:

```{prompt}
:language: bash
:prompts: xenia ~ \$
cd catalyst
```

Symlink the config directory:

```{prompt}
:language: bash
:prompts: xenia ~/catalyst \#
ln -sf $PWD/config /var/tmp/catalyst/
```

With catalyst setup, download a [stage3](https://gentoo.org/downloads). To find which one is needed, look in the spec file you are building under `source_subpath`. For openrc, it is usually a standard openrc stage3. For systemd, it is usually systemd-mergedusr.

Finally, {doc}`add the Xenia overlay </usage/xenia-overlay>` to your system.

## Create a snapshot

Run a portage sync:

```{prompt}
:language: bash
:prompts: xenia ~/catalyst \#
emerge-webrsync
```

Create the snapshot:

```{prompt}
:language: bash
:prompts: xenia ~/catalyst \#
catalyst -s stable
```

There will be an entry like the following:

```
NOTICE:catalyst:Wrote snapshot to /var/tmp/catalyst/snapshots/gentoo-202a513901854da4c0f3bce7cfd8242e8194689d.sqfs
```

Get the snapshot hash (in this case `202a513901854da4c0f3bce7cfd8242e8194689d`), and replace `[SNAPSHOT_HASH]` with the hash:

```
snapshot_treeish: 202a513901854da4c0f3bce7cfd8242e8194689d
```

## Pick a spec file

Xenia has multiple spec files. For example, there is `stage4-openrc.spec` which will build an openrc GNOME image of Xenia. Or, there is `stage4-systemd-plasma.spec`, which will build a systemd Plasma/KDE image of Xenia.

## Spec file setup

In the spec file selected earlier, change all instances of `[CATALYST_DIR]` with the directory the catalyst repo is - in this case `/home/xenia/catalyst/`:

```
stage4/root_overlay: /home/xenia/catalyst/overlay
stage4/fsscript: /home/xenia/catalyst/stage4-openrc.sh
```

## Building

Build the stage4:

```{note}
-a here means no auto-resume. We specify this in-case of an error which requires a full rebuild. Auto-resume may be handy if you need to stop for whatever reason and come back later, so feel free to remove this flag
``` 

```{prompt}
:language: bash
:prompts: xenia ~/catalyst \#
catalyst -a -f [your spec file].spec
```

```{note}
If there is any errors building, the first port of call is to download a new stage3. If this doesn't work, try running catalyst again without -a. This will get around any issues with post-install of packages (`gtk+` being the main culprit here).
```

The stage4 will now be in `/var/tmp/catalyst/builds/default/stage4-[arch]-[target].tar.xz`. The target will be the target set in the spec file. The arch will be the architecture of the image (amd64 is most common here, unless `stage4-arm64-*` is being used as a spec file).

### Notes

This process will be very slow the first time you build the stage4 - you will be compiling GCC, LLVM, a DE etc. However, catalyst keeps binary packages! So your next build will be very quick.

You can learn about customising your own rootFS {doc}`here </development/customise-root>`.

## Converting to squashFS

The stage4 you have made is currently a tarball. Let's convert it to a squashFS:

```{prompt}
:language: bash
:prompts: xenia ~/catalyst \#
rm root.img
mksquashfs /var/tmp/catalyst/tmp/default/stage4-[arch]-[target] root.img -comp xz
```

## Installation

First, host a server where your `root.img` is located:

```{prompt}
:language: bash
:prompts: xenia ~/catalyst \$
python3 -m http.server
```

```{note}
This part is unfinished! There will be better update documentation soon
```

If you have an existing installation, you can simply replace `root.img` in `/roots` with your own. If you don't, follow the {doc}`guided installation </installation/guided-install>`, replacing the root image URL with your own.
