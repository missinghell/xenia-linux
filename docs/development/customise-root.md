# Customising the rootfs

```{warning}
These docs are outdated, using catalyst for builds. Our current Docker build system hasn't been documented yet - help appreciated!
```

You can customise the rootFS with different packages, included files, USE flags - even different init systems. This page will give a small overview of what you can change.

## Requirements

- A catalyst directory, setup according to {doc}`manual rootFS generation </development/create-root>` or by {doc}`foxbuild </development/create-root-guided>`

```{note}
This page will reference files and directories relative to your catalyst directory. On `foxbuild`, this will be a fork of the catalyst repo.
``` 

```{note}
This guide uses the spec name `stage4-openrc.spec` as a placeholder for any spec name. Replace it with the spec being edited.
```

## Packages

To add, change or remove packages in Xenia, edit `stage4-openrc.spec`:

```
stage4/packages:
    app-admin/sudo
    ...
    sys-process/cronie
```

You can add your own packages here. Make sure they are spelled correctly and you include the package category.

## USE flags and other portage config

### Global USE flags

To add a global USE flag, edit `stage4-openrc.spec`:

```
stage4/use:
    -gnome-online-accounts
    ...
    video_cards_vmware
```

You can add USE flags here as well as removing USE flags with `-`.

### Package specific USE flags

To add package specific USE flags, edit `config/package.use/stage4`:

```
sys-boot/grub device-mapper
...
```

You can add USE flags here the same way you do in a typical `package.use`. You can also change package masks, licenses and keywords here (for example to enable testing on a specific package).

## Adding services

To add services to Xenia (the equivalent of `rc-update`), edit `stage4-openrc.spec`:

```
stage4/rcadd:
    cupsd|default
    ...
    udev|sysinit
```

You can specify the service here alongside the runlevel in the format `service|runlevel`.

On systemd specs, this is defined in the `fsscript`. See below for more details.

## Custom shell script

To run custom script at the end of the build process, edit `stage4-openrc.sh`:

```bash
#!/bin/bash
...
```

You can add your own custom script at the end of this file.

## Filesystem overlay

To add arbitrary files to Xenia, such as new backgrounds or configuration for packages, open `overlay` and add any files you want here. For example, to edit `/etc/fstab`, edit `overlay/etc/fstab`.

Multiple overlays can be added. See `stage4-systemd.spec` for an example. It applies both the common overlay that is shared amongst all spec files and a GNOME specfic overlay.

These files will be added after catalyst has completed emerging the system.

## Submitting changes to Xenia

If you make a change and you want it merged into Xenia, please follow these guidelines:

- Have a defined purpose to all your changes and reasoning for why it should be added to the base system
- Make sure added files, packages and USE flags are not specific to your system
- Put the services, packages and global USE flags in alphabetical order

You can then submit an MR to the [catalyst repository](https://gitlab.com/xenia-group/catalyst) with your changes.