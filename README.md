# Xenia Linux

<div align="center"><a href="https://wiki.xenialinux.com/">Wiki</a> - <a href="https://blog.xenialinux.com/">Blog</a> - <a href="https://xenialinux.com/">Website</a></div><br>

![Screenshot of Xenia Linux](https://gitlab.com/xenia-group/branding/-/raw/main/screenshots/0.4-main.png)

Xenia Linux is an immutable OS based on Gentoo Linux. Xenia Linux boots from a dracut initramfs, and boots from a SquashFS (the root), much like how a LiveCD works.

Xenia Linux includes the GNOME desktop environment, a selection of filesystems, and versatility with Flatpak, Distrobox and Portage available to install packages with. Xenia Linux includes your entire system on one file, which can be swapped out for a newer one when updates come.

We named Xenia Linux after the popular Linux mascot who lost to Tux.

## How do I get it?

Follow the [installation instructions](https://wiki.xenialinux.com/en/latest/installation/guided-install.html) to install Xenia Linux.

### How do I install programs?

![Screenshot of Xenia Linux, with distrobox open displaying the neofetch of multiple distros and an about page of a Firefox window running in Flatpak](https://gitlab.com/xenia-group/branding/-/raw/main/screenshots/0.4-packaging.png)

Follow the [documentation](https://wiki.xenialinux.com/en/latest/usage/installing-apps.html) to learn how to install applications.

## A thank you

Thank you to [Jack](https://gitlab.com/ArykDev) (our wonderful co-founder), Immoloism and the many people over at [It Runs Gentoo](
https://discord.gg/2VCEjaXqEe) for help (and emotional support) while making this distro. Thanks for those over at #gentoo-releng, ##gnu/crack and others for help with various issues. It wouldn't be possible without them. - Luna

But more importantly, thank you to YOU for trying Xenia Linux. 

If you ever need help, feel free to place issues here. And if you want to contribute, you are more than welcome to give us ideas or even make merge requests if you want to contribute technically to the project!